# 1D ASCII output created by CarpetIOASCII
# created on redshift.wireless.rit.edu by eschnett on Oct 25 2012 at 16:53:21-0400
# parameter filename: "/Users/eschnett/Cvanilla/arrangements/Carpet/Carpet/test/kasner.par"
#
# ADMBASE::METRIC d (admbase::metric)
#
# iteration 0   time 1
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:gxx 14:gxy 15:gxz 16:gyy 17:gyz 18:gzz
#
#
#
0	0 0 0 0	3 3 3	1	-5 -5 -5	1 0 0 1 0 1
0	0 0 0 0	4 4 4	1	-4 -4 -4	1 0 0 1 0 1
0	0 0 0 0	5 5 5	1	-3 -3 -3	1 0 0 1 0 1
0	0 0 0 0	6 6 6	1	-2 -2 -2	1 0 0 1 0 1
0	0 0 0 0	7 7 7	1	-1 -1 -1	1 0 0 1 0 1
0	0 0 0 0	8 8 8	1	0 0 0	1 0 0 1 0 1
#
#
#
#
#
#
#
#

# iteration 0   time 1
# time level 1
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:gxx 14:gxy 15:gxz 16:gyy 17:gyz 18:gzz
#
#
#
0	1 0 0 0	3 3 3	1	-5 -5 -5	0.868940446145067 0 0 0.868940446145067 0 1.07276598289514
0	1 0 0 0	4 4 4	1	-4 -4 -4	0.868940446145067 0 0 0.868940446145067 0 1.07276598289514
0	1 0 0 0	5 5 5	1	-3 -3 -3	0.868940446145067 0 0 0.868940446145067 0 1.07276598289514
0	1 0 0 0	6 6 6	1	-2 -2 -2	0.868940446145067 0 0 0.868940446145067 0 1.07276598289514
0	1 0 0 0	7 7 7	1	-1 -1 -1	0.868940446145067 0 0 0.868940446145067 0 1.07276598289514
0	1 0 0 0	8 8 8	1	0 0 0	0.868940446145067 0 0 0.868940446145067 0 1.07276598289514
#
#
#
#
#
#
#
#

# iteration 0   time 1
# time level 2
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:gxx 14:gxy 15:gxz 16:gyy 17:gyz 18:gzz
#
#
#
0	2 0 0 0	3 3 3	1	-5 -5 -5	0.742654213378045 0 0 0.742654213378045 0 1.16039720840319
0	2 0 0 0	4 4 4	1	-4 -4 -4	0.742654213378045 0 0 0.742654213378045 0 1.16039720840319
0	2 0 0 0	5 5 5	1	-3 -3 -3	0.742654213378045 0 0 0.742654213378045 0 1.16039720840319
0	2 0 0 0	6 6 6	1	-2 -2 -2	0.742654213378045 0 0 0.742654213378045 0 1.16039720840319
0	2 0 0 0	7 7 7	1	-1 -1 -1	0.742654213378045 0 0 0.742654213378045 0 1.16039720840319
0	2 0 0 0	8 8 8	1	0 0 0	0.742654213378045 0 0 0.742654213378045 0 1.16039720840319
#
#
#
#
#
#
#
#

# iteration 0   time 1
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:gxx 14:gxy 15:gxz 16:gyy 17:gyz 18:gzz
#
#
#
#
#
#
#
#
#
0	0 0 1 0	9 9 9	1	1 1 1	1 0 0 1 0 1
0	0 0 1 0	10 10 10	1	2 2 2	1 0 0 1 0 1
0	0 0 1 0	11 11 11	1	3 3 3	1 0 0 1 0 1
0	0 0 1 0	12 12 12	1	4 4 4	1 0 0 1 0 1
0	0 0 1 0	13 13 13	1	5 5 5	1 0 0 1 0 1
#
#
#

# iteration 0   time 1
# time level 1
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:gxx 14:gxy 15:gxz 16:gyy 17:gyz 18:gzz
#
#
#
#
#
#
#
#
#
0	1 0 1 0	9 9 9	1	1 1 1	0.868940446145067 0 0 0.868940446145067 0 1.07276598289514
0	1 0 1 0	10 10 10	1	2 2 2	0.868940446145067 0 0 0.868940446145067 0 1.07276598289514
0	1 0 1 0	11 11 11	1	3 3 3	0.868940446145067 0 0 0.868940446145067 0 1.07276598289514
0	1 0 1 0	12 12 12	1	4 4 4	0.868940446145067 0 0 0.868940446145067 0 1.07276598289514
0	1 0 1 0	13 13 13	1	5 5 5	0.868940446145067 0 0 0.868940446145067 0 1.07276598289514
#
#
#

# iteration 0   time 1
# time level 2
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:gxx 14:gxy 15:gxz 16:gyy 17:gyz 18:gzz
#
#
#
#
#
#
#
#
#
0	2 0 1 0	9 9 9	1	1 1 1	0.742654213378045 0 0 0.742654213378045 0 1.16039720840319
0	2 0 1 0	10 10 10	1	2 2 2	0.742654213378045 0 0 0.742654213378045 0 1.16039720840319
0	2 0 1 0	11 11 11	1	3 3 3	0.742654213378045 0 0 0.742654213378045 0 1.16039720840319
0	2 0 1 0	12 12 12	1	4 4 4	0.742654213378045 0 0 0.742654213378045 0 1.16039720840319
0	2 0 1 0	13 13 13	1	5 5 5	0.742654213378045 0 0 0.742654213378045 0 1.16039720840319
#
#
#


