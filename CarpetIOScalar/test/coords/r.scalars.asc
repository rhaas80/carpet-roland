# Scalar ASCII output created by CarpetIOScalar
# created on horizon.tapir.caltech.edu by rhaas on Mar 03 2012 at 16:38:13-0800
# parameter filename: "coords.par"
#
# GRID::r (r)
# 1:iteration 2:time 3:data
# data columns: 3:r(average) 4:r(iaverage) 5:r(norm_inf) 6:r(inorm1) 7:r(inorm2) 8:r(maximum) 9:r(minimum) 10:r(norm1) 11:r(norm2)
0 0 0.488709669281446 0.488709669281446 0.866025403784439 0.488709669281446 0.510524393972035 0.866025403784439 0.0714285714285714 0.488709669281446 0.510524393972035
